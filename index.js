const Axios = require("axios");
const Cheerio = require("cheerio");
const FileSystem = require("fs");
const Path = require("path");
const BASE_URL = 'https://accordiluminacao.com';
const PAGE_URL = BASE_URL + "/en/products";

crawler();
// downloadProjetos(imgs());

async function crawler() {
  const produtos = [];
  for (let i = 1; i < 5; i++) {
    const links = await getUrlProdutos(i);
    links.forEach((l) => produtos.push(l));
  }

  // const result = [];

  for (const produto of produtos) {
    const item = await montaProduto(produto);
    await armazenaFotosProduto(item);
    // result.push(item);
  }

  // gerarCSV(result);
  // armazenaFotos(result);
}

function gerarCSV(produtos) {
  console.log("descricao;ref;altura;largura;profundidade;diametro");
  for (const produto of produtos) {
    for (const dimensao of produto.referenciasFormatadas) {
      console.log(
        `${produto.descricao};${dimensao.referencia};${dimensao.altura};${dimensao.largura};${dimensao.profundidade};${dimensao.diametro}`
      );
    }
  }
}

async function armazenaFotos(produtos) {
  for (const produto of produtos) {
    await armazenaFotosProduto(produto);
  }
}

async function armazenaFotosProduto(produto) {
  console.log('Amazenando imagens produto: ', produto.descricao);
  const dirname = __dirname + '\\imgs\\' + produto.url.substring(produto.url.indexOf('produto/') + 'produto/'.length);
  const existePasta = FileSystem.existsSync(dirname)
    if (!existePasta) {
      console.log('Criando Pasta', dirname);
      FileSystem.mkdirSync(dirname);
    }
    try {

      if(!produto.imagens) {
        console.log(`PRODUTO ${produto.descricao} SEM IMAGENS!!!`)
        return
      }

      for (const img of produto.imagens) {      
          const descricaoImagem = img.substring(img.indexOf('produtos/') + 'produtos/'.length).replace('/','-');
          const path = Path.resolve(dirname, descricaoImagem);

          if (path.includes("gens-ambientes")) {
            continue;
          }
          const writer = FileSystem.createWriteStream(path);
          const response = await Axios({
            url: BASE_URL + '/' + img,
            method: 'GET',
            responseType: 'stream'
          })
          console.log('Escrevendo arquivo: ', img);
          response.data.pipe(writer)
        
      }
    } catch(e) {
      console.error('ERRO:', e);
    }
    
}

async function downloadProjetos(imagens) {
  var i = 0;
  for (const img of imagens) {
    try {
      const path = Path.resolve(__dirname + '/projetos', `${i++}.jpeg`);
      const writer = FileSystem.createWriteStream(path);
      const response = await Axios({
        url: img,
        method: 'GET',
        responseType: 'stream'
      })
      console.log('Escrevendo arquivo: ', img);
      response.data.pipe(writer)
    } catch(e) {
      console.error('ERRO:', e);
    }
    
  }
}

async function getUrlProdutos(pagination) {
  console.log('Buscando URLs pagina: ', pagination);
  const response = await Axios.get(PAGE_URL + `?page=${pagination}`);
  return getUrlProduto(response.data);
}

function getUrlProduto(body) {
  var $ = Cheerio.load(body);
  var linksPordutos = $("#list-produtos > div > a");
  var links = [];
  linksPordutos.map((_, element) => {
    links.push(`${element.attribs["href"]}`);
  });

  return links;
}

async function montaProduto(produtoUrl) {
  console.log('Motando produto: ', produtoUrl);
  
  var produto = {};
  try {
    produto.url = produtoUrl;
    var response = await Axios.get(produto.url);
    var page = Cheerio.load(response.data);

    produto.descricao = getDescricao(page);
    produto.imagens = getUrlImagens(page);
    // produto.referencias = getReferencias(page);
    // produto.dimensoes = getDimensoes(page);
    // produto.referenciasFormatadas = getReferenciasAjustadas(produto.referencias, produto.dimensoes);
  } catch(e) {
    console.error(e);
  }  
  return produto;
}

function getReferenciasAjustadas(referencias, dimensoes) {
  var ajustadas = [];
  for (let index = 0; index < referencias.length; index++) {
    var r = referencias[index];
    var d = dimensoes[index];
    ajustadas.push(getDimensoesFormatadas(r, d));
  }

  return ajustadas;
}

function getUrlImagens($) {
  var imgs = $(".img-produto-carousel");
  var links = [];
  imgs.map((_, element) => {
    var src = element.attribs["src"];
    if(src) {
      links.push(`${element.attribs["src"]}`);
    }
  });

  return links;
}

function getReferencias($) {
  var options = $("#ref > option");
  var referencias = [];
  options.map((_, option) => {
    var content = option.attribs["data-content"];
    var inicio = content.indexOf("<span>") + "<span>".length;
    var fim = content.indexOf("</span>");
    referencias.push(content.substring(inicio, fim));
  });

  return referencias;
}

function getDimensoes($) {
  var divs = $("#carouselLamps > div > div");
  var referenciaDimencoes = [];
  divs.map((_, div) => {
    var h6s = [];
    var ps = [];
    var dimensoes = [];

    Cheerio.load(div)("h6").map((_, item) => h6s.push(item.firstChild.data));

    Cheerio.load(div)("p").map((_, item) => ps.push(item.firstChild.data));

    for (let i = 0; i < h6s.length; i++) {
      dimensoes.push({
        tipo: h6s[i],
        valor: ps[i],
      });
    }
    referenciaDimencoes.push(dimensoes);
  });

  return referenciaDimencoes;
}

function getDimensoesFormatadas(referencia, dimensoes) {
  var df = {
    referencia,
    altura: null,
    largura: null,
    profundidade: null,
    diametro: null,
  };
  for (const dimensao of dimensoes) {
    if (dimensao.tipo === "Altura") {
      df.altura = formataValorDimensao(dimensao.valor);
    }
    if (dimensao.tipo === "Largura") {
      df.largura = formataValorDimensao(dimensao.valor);
    }
    if (dimensao.tipo === "Profundidade") {
      df.profundidade = formataValorDimensao(dimensao.valor);
    }
    if (dimensao.tipo === "Diâmetro") {
      df.diametro = formataValorDimensao(dimensao.valor);
    }
  }

  return df;

  function formataValorDimensao(valor) {
    return valor.replace("cm", "").replace(".", ",").trim();
  }
}

function getDescricao($) {
  var h1s = $('h1[class="h4 font-weight-bold"]');
  var descricao = "";

  h1s.map((_, item) => (descricao = item.firstChild.data));

  return descricao;
}


function imgs() {
  return ["https://accordiluminacao.com/cdn/imagens/ambientes/xSQorur6pXTCuPlUzo1l011ESjQWly9G4h0Ct7nK.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/nn8BW7228As9LWZ5cwFFZKHhMI2WiA4yezBsoDtg.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/86F9VXWWylZwiy9AIYDtM71GMGYaq762m1jcivbS.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/RBI1yI01KVgGGaPx0jmG1XjDC42B6Z0eR2I7cxGZ.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/AhFXzKGGepDHJSE7ubsryuCVYg1kavtkD0qz5eT7.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/e1bTIrp8p0hG8KnOyTOovYbidd4UKe14igbOuiIt.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/BrvlHg1uKQ07MxlzTgpCkM7a2AongsPAoVDeb00f.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/O8r7FJLAl5UX0qL9X7aJT6MlYDxjcjeQu9rSMixY.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/fyEXGfMRnuthL7o7MziU2cFSGQlDqAPLyvafdirR.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/ASZy6ZgrT2dvmzRa3QFWeb7KJNme8w0Gv6XIDZmf.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/ldw59NDd0ln9GLH0FPSamtGuGhsT5U54dzUYVzfA.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/SAWNnXhwcWPnXuKpfurRTkS8iABJQjM796kbZ9TR.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/zglf7xTMgM13zP5hyJW27gFFhuFcGkWxMZeIRv6I.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/crgWE6L3HCoRPAyF6gtSrcqjDouNByL5lab5BhF4.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/eNiY0OlZUWA8sr7w3Myv2uj4FimpRX60CXIzqUJ3.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/0TFgAdrGyBm9xaxtQcbOtq45UOqgZYEtTyo11xXh.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/uoKJWUHwzMNWm7i1CUmXLCmsB5hUpitimjUZtTRA.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/6XutuDbDASRvKAKZbRbDXDHO4in5sLtxzWZyxVjs.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/ZvO1wAR2vU9kT4RmGbOUwNX9QJ7cThY4S4LWB1dZ.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/3TqdZWXZvhoNxpn4dh2J1icGFc1HXMAHE2AqGchU.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/JPYoC1OLsaN2mSVHlNlBPKjL8fKRytlBDsV3FpLu.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/i3QiE6fgKOW8GIWVNtI98WlD4b2dl7wufEEjzbkc.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/FdNssYT3Km5SThU2cmuBHmbJ3N71fYkIDp5IuTwl.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/qNE4TleuRPlsyxvjrnm4jlP5SDhFEzmkY3btqjFT.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/eZcbXvpsaMJ7QDWfEdAceoGL5xIXqXaQI9noJoIr.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/2aZdPyOmin8cxmybNLbjIkp1gB6kGHOCtxoRw35M.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/K152e8n1EUMztKrs6WOhIGUDVaNLqk1l28XIJOLZ.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/6GHKud0QZhs7gfkN8Nouj1jo9Z2UUNeCXoEk6z1E.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/YkLeixJPfe0BvoDJBwKMN4YVDx4IbdsH1ilsALxN.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/Iiox9HgROz93ZR4LSXHKe1lhUQGAry8tmaUW79Jr.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/DcFoqNefQZ7BU344HxMMjVLMRh46TDTdtEPWMN0q.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/np3YGkvyjIwo8dk2yjsNClL0px81BQGdtJDWF6Qu.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/sYKBxhVAZQEokxzPAmIZRb44H2uCnKZ84QocXzvv.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/jYzng5eV85Ssj5nrtdKk4sIS0dxAGpbhmMXpWH8M.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/EmgY0NCFhF6eSvbNAOr3s4PStB9Zb2Mm12zUygXm.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/GrkC2yfqBzHPJq15RhbrF80LXyudBEh1Iitpg5wW.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/6dl8mNIDe5sZJf3fuaE90hIzSoVAKNPvOnjSrxS1.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/ie09kqBpajwc89gYvQryEkFdxkZijhy7UDmGrtQE.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/EgkJ007is4aSM0zH0nIJMaikRraQ0ln9enqnm54x.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/sSlKYf29x2x17Il8RIXSFPHfADP0bJPrTgaIGhB3.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/0nzenUIiGRXpEofQVt5fzApENQsWO9dCUx5m4spT.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/7jO7KHizEqtW8EunjqW18sN9OakNuKGfYhMc7DyZ.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/uH9utWKqwoKB89O9y0vSw1icZHzvtL1iw4DMUspy.jpeg",
  "https://accordiluminacao.com/cdn/imagens/ambientes/5OKO8jWMXvTgcHey92ZTYCv7OPemMHVR0adpuVIs.jpeg"]
}