descricao;ref;altura;largura;profundidade;diametro
Arandela Accord Clean;4133;50;6,5;6;null
Arandela Accord Clean;4132;45;6,9;4,8;null
Arandela Accord Clean;4130;42;6;5,5;null
Arandela Accord Clean;4131;45;5;4,7;null
Arandela Accord Fuchsia;4136;22,5;19;21;null
Arandela Accord Cônica;4138;17,5;15;20;null
Arandela Accord Clean;4134;0;null;2,5;25
Pendente Accord Dot;1420;82;90;170;30
Pendente Accord Dot;1421;110;30;108;30
Pendente Accord Dot;1419;110;110;110;30
Pendente Accord Dot;1418;152;29;null;30
Arandela Accord Dot;4127;127;24;null;30
Arandela Accord Dot;4129;36;24;null;25
Arandela Accord Dot;4128;49;24;null;25
Abajur Accord Dot;7057;46;25;null;25
Coluna Accord Dot;3126;164;30;null;30
Abajur Accord Sakura;7058;50,5;38;22;null
Coluna Accord Sakura;3127;160;46;56;null
Pendente Accord Sakura;1422;105;81,5;96;null
Arandela Accord Sakura;4135;61,5;43;22,5;null
Coluna Accord Curi;3124;175;null;null;70
Coluna Accord Curi;3125;175;null;null;90
Abajur Accord Curi;7055;29;null;null;30
Abajur Accord Curi;7056;46;null;null;40
Arandela Accord Curi;4126;0;null;7;30
Plafon Accord Curi;5087;11;null;null;70
Plafon Accord Curi;5088;13;null;null;90
Pendente Accord Curi;1389;6;null;null;70
Pendente Accord Curi;1390;6;null;null;90
Pendente Accord Curi;1391;6;null;null;70
Pendente Accord Curi;1392;10;null;null;90
Arandela Accord Frame;5084;10;70;126,5;null
Arandela Accord Frame;5085;10;98;130;null
Arandela Accord Frame;5086;10;125;155;null
Arandela Accord Frame;5081;10;66;66;null
Arandela Accord Frame;5082;10;89;89;null
Arandela Accord Frame;5083;10;112;112;null
Arandela Accord Frame;4118;30;15;2,5;null
Arandela Accord Frame;4116;25;25;2,5;null
Arandela Accord Frame;4117;30;30;2,5;null
Pendente Accord Frame;1386;60;30;30;null
Pendente Accord Frame;1387;80;40;40;null
Pendente Accord Frame;1388;99;50;50;null
Pendente Accord Frame;1381;60;30;2,5;null
Pendente Accord Frame;1382;70;35,5;2,5;null
Pendente Accord Frame;1383;80;40;2,5;null
Pendente Accord Frame;1384;90;45;2,5;null
Pendente Accord Frame;1385;99;51;2,5;null
Coluna Accord Frame;3123;160;10;13;null
Abajur Accord Frame;7054;30;30;6;null
Abajur Accord Frame;7053;25;25;6;null
Plafon Accord Frame;5081;10;66;66;null
Plafon Accord Frame;5082;10;89;89;null
Plafon Accord Frame;5083;10;112;112;null
Plafon Accord Frame;5071;4,5;60;60;null
Plafon Accord Frame;5072;4,5;70;70;null
Plafon Accord Frame;5073;4,5;80;80;null
Plafon Accord Frame;5074;4,5;90;90;null
Plafon Accord Frame;5075;4,5;100;100;null
Plafon Accord Frame;5076;4,5;30;60;null
Plafon Accord Frame;5077;4,5;35;70;null
Plafon Accord Frame;5078;4,5;40;80;null
Plafon Accord Frame;5079;4,5;45;90;null
Plafon Accord Frame;5080;4,5;50;100;null
Pendente Accord Frame;1366;2,5;60;60;null
Pendente Accord Frame;1367;2,5;70;70;null
Pendente Accord Frame;1368;2,5;80;80;null
Pendente Accord Frame;1369;2,5;90;90;null
Pendente Accord Frame;1370;2,5;100;100;null
Pendente Accord Frame;1376;2,5;30;60;null
Pendente Accord Frame;1377;2,5;35;70;null
Pendente Accord Frame;1378;2,5;40;80;null
Pendente Accord Frame;1379;2,5;45;90;null
Pendente Accord Frame;1380;2,5;50;100;null
Plafon Accord Frame;5066;4,5;null;null;60
Plafon Accord Frame;5067;4,5;null;null;70
Plafon Accord Frame;5068;4,5;null;null;80
Plafon Accord Frame;5069;4,5;null;null;90
Plafon Accord Frame;5070;4,5;null;null;100
Plafon Accord Frame;5084;10;70;126,5;null
Plafon Accord Frame;5085;10;98;130;null
Plafon Accord Frame;5086;10;125;155;null
Abajur Accord Frame;7052;30;10;6;null
Pendente Accord Frame;1371;60;60;2,5;null
Pendente Accord Frame;1372;70;70;2,5;null
Pendente Accord Frame;1373;80;80;2,5;null
Pendente Accord Frame;1374;90;90;2,5;null
Pendente Accord Frame;1375;100;100;2,5;null
Arandela Accord Linea;4099;40;4,5;4,5;null
Arandela Accord Linea;4100;50;4,5;4,5;null
Arandela Accord Linea;4101;60;4,5;4,5;null
Arandela Accord Linea;4102;70;4,5;4,5;null
Arandela Accord Linea;4103;80;4,5;4,5;null
Arandela Accord Linea;4104;90;4,5;4,5;null
Arandela Accord Linea;4105;100;4,5;4,5;null
Arandela Accord Linea;4106;110;4,5;4,5;null
Arandela Accord Linea;4107;120;4,5;4,5;null
Arandela Accord Linea;4108;130;4,5;4,5;null
Arandela Accord Linea;4109;140;4,5;4,5;null
Arandela Accord Linea;4110;150;4,5;4,5;null
Arandela Accord Linea;4111;160;4,5;4,5;null
Arandela Accord Linea;4112;170;4,5;4,5;null
Arandela Accord Linea;4113;180;4,5;4,5;null
Arandela Accord Linea;4114;190;4,5;4,5;null
Arandela Accord Linea;4115;200;4,5;4,5;null
Arandela Accord Linea;4082;40;4,5;4,5;null
Arandela Accord Linea;4083;50;4,5;4,5;null
Arandela Accord Linea;4084;60;4,5;4,5;null
Arandela Accord Linea;4085;70;4,5;4,5;null
Arandela Accord Linea;4086;80;4,5;4,5;null
Arandela Accord Linea;4087;90;4,5;4,5;null
Arandela Accord Linea;4088;100;4,5;4,5;null
Arandela Accord Linea;4089;110;4,5;4,5;null
Arandela Accord Linea;4090;120;4,5;4,5;null
Arandela Accord Linea;4091;130;4,5;4,5;null
Arandela Accord Linea;4092;140;4,5;4,5;null
Arandela Accord Linea;4093;150;4,5;4,5;null
Arandela Accord Linea;4094;160;4,5;4,5;null
Arandela Accord Linea;4095;170;4,5;4,5;null
Arandela Accord Linea;4096;180;4,5;4,5;null
Arandela Accord Linea;4097;190;4,5;4,5;null
Arandela Accord Linea;4098;200;4,5;4,5;null
Coluna Accord Fuchsia;3122;160;null;null;30
Abajur Accord Fuchsia;7050;63;null;null;20
Pendente Accord Fuchsia;1365;21;null;null;94
Abajur Accord Fuchsia;7049;25;null;null;34
Pendente Accord Fuchsia;1363;43;null;null;31
Pendente Accord Fuchsia;1364;40;null;null;60
Abajur Accord Facetado;7048;62;null;null;30
Coluna Accord Facetada;3034;170;null;null;55
Pendente Accord Facetado;1360;6;null;null;60
Pendente Accord Facetado;1361;6;null;null;80
Pendente Accord Facetado;1362;6;null;null;100
Pendente Accord Facetado;1357;12;null;null;60
Pendente Accord Facetado;1358;12;null;null;80
Pendente Accord Facetado;1359;12;null;null;100
Pendente Accord Facetado;1354;6;null;null;60
Pendente Accord Facetado;1355;6;null;null;80
Pendente Accord Facetado;1356;6;null;null;100
Abajur Accord Facetado;7051;71;null;null;40
Arandela Accord Facetada;4064CO;16;16;5,2;null
Arandela Accord Facetada;4063CO;16;16;5,2;null
Arandela Accord Facetada;4079;54;46,5;6,5;null
Pendente Accord Frame;1415;3;null;3;40
Pendente Accord Frame;1316;3;null;3;60
Pendente Accord Frame;1317;3;null;3;70
Pendente Accord Frame;1318;3;null;3;80
Pendente Accord Frame;1319;3;null;3;90
Pendente Accord Frame;1320;3;null;3;100
Pendente Accord Frame;1294;3;null;3;60
Pendente Accord Frame;1295;3;null;3;70
Pendente Accord Frame;1296;3;null;3;80
Pendente Accord Frame;1297;3;null;3;90
Pendente Accord Frame;1298;3;null;3;100
Pendente Accord Cilíndrico;1349;12;null;6;60
Pendente Accord Cilíndrico;1350;12;null;6;70
Pendente Accord Cilíndrico;1351;12;null;6;80
Pendente Accord Cilíndrico;1352;12;null;6;90
Pendente Accord Cilíndrico;1353;12;null;6;100
Pendente Accord Orgânico;1325;5;80;6;null
Pendente Accord Orgânico;1326;5;100;6;null
Pendente Accord Orgânico;1327;5;120;6;null
Pendente Accord Orgânico;1328;5;150;6;null
Pendente Accord Orgânico;1345;12;80;6;null
Pendente Accord Orgânico;1346;12;100;6;null
Pendente Accord Orgânico;1347;12;120;6;null
Pendente Accord Orgânico;1348;12;150;6;null
Pendente Accord Oval;1321;5;null;7;70
Pendente Accord Oval;1322;5;null;7;80
Pendente Accord Oval;1323;5;null;7;90
Pendente Accord Oval;1324;5;null;7;100
Pendente Accord Orgânico;1329;5;80;6;null
Pendente Accord Orgânico;1330;5;97;6;null
Pendente Accord Orgânico;1331;5;114;6;null
Pendente Accord Orgânico;1332;5;140;6;null
Coluna Accord Spin;3029;164;null;null;70
Abajur Accord Spin;7044;73;null;null;40
Pendente Accord Canoa;1279 LED;16;120;20;null
Pendente Accord Canoa;1289 LED;16;150;20;null
Pendente Accord Canoa;1230 LED;16;190;20;null
Pendente Accord Onda;1300;17;160;20,5;null
Abajur Accord Physalis;7038;65;43;null;null
Arandela Accord Facetada;4077;100;26;13;null
Arandela Accord Facetada;4076;102;86;8,5;null
Arandela Accord Facetada;4074;97;72,5;8,5;null
Arandela Accord Facetada;4075;77;62,5;6,5;null
Pendente Accord Stecche Di Legno;1275;18;null;null;7,3
Pendente Accord Stecche Di Legno;1274;16;null;null;8,5
Pendente Accord Cônico;1233;30;null;null;16
Pendente Accord Stecche Di Legno;1282;2,2;25;10,4;null
Pendente Accord Barril;269;50;null;null;35
Pendente Accord Barril;1153;35;null;null;20
Pendente Accord Capadócia;1189;45;null;null;27
Pendente Accord Capadócia;1190;60;null;null;32
Pendente Accord Capadócia;1191;77;null;null;38
Pendente Accord Capadócia;1284;120;null;null;50
Pendente Accord Capadócia;1264;172;null;null;69
Pendente Accord Capadócia;1265;202;null;null;69
Arandela Accord Facetada;4063;16;16;5,2;null
Pendente Accord Orgânico;283;20;72;36;null
Pendente Accord Cônico;255;35;null;null;75
Pendente Accord Cônico;258;35;null;null;100
Pendente Accord Spy;1062;20;133;84;null
Pendente Accord Sfera;620;35;null;15;null
Pendente Accord Cônico;295;70;null;null;82
Pendente Accord Capadócia;1310;60;null;null;32
Pendente Accord Capadócia;1309;77;null;null;38
Pendente Accord Capadócia;1287;120;null;null;49
Pendente Cônico;1146;61;null;null;50
Pendente Accord Clean;1311 LED;11;70;4;null
Pendente Accord Clean;1312 LED;11;98;4;null
Pendente Accord Clean;1313 LED;11;127;4;null
Pendente Accord Clean;1314 LED;11;155;4;null
Pendente Accord Clean;1315 LED;11;183;4;null
Pendente Accord Oval;287;25;82;45;null
Pendente Accord Oval;1218;25;80;40;null
Pendente Accord Cônico;1100;17,5;null;null;14,5
Pendente Accord Cônico;1151;19;null;null;16
Pendente Accord Cônico;264;33;null;null;27,5
Pendente Accord Cristais;1130C;25;null;null;20
Pendente Accord Cônico;296;25;null;null;62
Pendente Accord Cônico;1145;28;null;null;45
Pendente Accord Cônico;1250;30;null;null;16
Abajur Accord Cônico;7047;67;null;null;40
Pendente Accord Cônico;1276;50;null;null;10
Pendente Accord Cônico;1277;70;null;null;15
Pendente Accord Cônico;1280;70;null;null;15
Pendente Accord Cônico;116;30;null;null;12
Pendente Accord Cilíndrico;1285 LED;12;null;null;60
Pendente Accord Cilíndrico;1286 LED;12;null;null;70
Pendente Accord Cilíndrico;1221 LED;12;null;null;80
Plafon Accord Facetado;5065;20;null;null;50
Pendente Accord Cilíndrico;1285CO LED;12;null;null;60
Pendente Accord Cilíndrico;1286CO LED;12;null;null;70
Pendente Accord Cilíndrico;1221CO LED;12;null;null;80
Abajur Accord Sfera;7043;0;25;null;35
Pendente Accord Cilíndrico;215 F;25;null;null;60
Pendente Accord Cilíndrico;213 F;25;null;null;80
Pendente Accord Sfera;1307;35;73;15;null
Pendente Accord Cilíndrico;215;25;null;null;60
Pendente Accord Cilíndrico;213;25;null;null;80
Pendente Accord Sfera;1308;35;null;15;null
Pendente Accord Cristais;1198;130;null;null;80
Pendente Accord Cristais;215C;60,9;null;null;60
Pendente Accord Cristais;213C;69;null;null;80
Pendente Accord Cristais;298C;95,2;null;null;120
Pendente Accord Cristais;1155C;97,2;null;null;150
Pendente Accord Cristais;1142;36;85;30;null
Pendente Accord Cristais;1143;36;130;30;null
Arandela Accord Onda;4072;120;20,5;15;null
Pendente Accord Physalis;1299;15;null;null;35
Pendente Accord Physalis;1283;20;null;null;47
Pendente Accord Physalis;1281;25;null;null;66
Pendente Accord Cilíndrico;214;18;null;null;40
Pendente Accord Cilíndrico;202;25;null;null;50
Pendente Accord Cilíndrico;206;25;null;null;60
Pendente Accord Cilíndrico;231;15;null;null;50
Pendente Accord Cilíndrico;1037;15;null;null;60
Pendente Accord Cilíndrico;1038;15;null;null;70
Pendente Accord Cilíndrico;1039;15;null;null;80
Pendente Accord Cilíndrico;1040;15;null;null;90
Pendente Accord Cilíndrico;1041;15;null;null;100
Pendente Accord Cilíndrico;207;25;null;null;70
Pendente Accord Cilíndrico;204;25;null;null;80
Pendente Accord Cilíndrico;217;25;null;null;90
Pendente Accord Cilíndrico;205;25;null;null;100
Pendente Accord Facetado;1290;16;null;null;35
Pendente Accord Facetado;1291;23;null;null;50
Coluna Accord Jangada;3015;204;53;30;null
Coluna Accord Physalis;3020;200;100;null;null
Pendente Accord Orgânico;1200;20;null;null;116
Coluna Accord Clean;3019;163;null;null;60
Coluna Accord Stecche Di Legno;363;160;null;null;70
Pendente Accord KS;1216;20;80;55;null
Pendente Accord KS;291;20;100;68;null
Pendente Accord KS;263;20;120;84;null
Coluna Accord Ripada;361/2;160;null;null;50
Coluna Accord Ripada;361;170;null;null;55
Coluna Accord Capadócia;3033;120;null;null;50
Coluna Accord Capadócia;3007;172;null;null;69
Coluna Accord Capadócia;3008;202;null;null;69
Pendente Accord Cônico;1130;25;null;null;20
Pendente Accord Orgânico;1337;20;150;64;null
Pendente Accord Orgânico;1258;20;200;85;null
Pendente Accord Orgânico;285;20;85;52;null
Coluna Accord Clean;3003 Cúpula Madeira;160;null;null;60
Coluna Accord Clean;3003 Cúpula Linho;160;null;null;70
Pendente Accord Trevo;1222;20;120;120;null
Coluna Accord Lanterna;3025;65;35;35;null
Coluna Accord Lanterna;3011;77;35;35;null
Coluna Accord Lanterna;3012;102;45;45;null
Coluna Accord Cônica;3004;161;null;null;70
Coluna Accord Pontas;368;98;null;null;23
Arandela Accord Sfera;4073;0;null;16;35
Abajur Accord Infinito;7012;74;null;null;40
Abajur Accord Diamante;7035;67;null;null;40
Abajur Accord Diamante;7034;70;null;null;40
Abajur Accord Capadócia;7022;86;null;null;60
Abajur Accord Capadócia;7022/M;86;null;null;60
Abajur Accord Capadócia;7021;80;null;null;38
Abajur Accord Stecche Di Legno;7020;58;null;null;40
Abajur Accord Stecche Di Legno;7018;98;null;null;70
Abajur Accord Ripado;7014;66;null;null;40
Abajur Accord Ripado;7014/M;66;null;null;40
Abajur Accord Clean;7037;70;null;null;40
Abajur Accord Cônico;7026 Cúpula Linho;61;null;null;40
Abajur Accord Cônico;7026 Cúpula Madeira;61;null;null;40
Abajur Accord Barril;7011;50;null;null;15
Abajur Accord Facetado;7030;54;null;null;40
Abajur Accord Clean;1024;45;30;15;null
Abajur Accord Stecche Di Legno;7013;64;null;null;40
Abajur Accord Sfera;141;0;25;null;35
Abajur Accord Cilíndrico;145;38,5;null;null;40
Plafon Accord KS;5043;20;120;83;null
Plafon Accord Orgânico;5064;20;83;53;null
Plafon Accord Orgânico;585;15;null;null;65
Plafon Accord Orgânico;589;15;null;null;75
Plafon Accord Cilíndrico;504;12;null;null;35
Plafon Accord Cilíndrico;529;12;null;null;40
Plafon Accord Cilíndrico;528;12;null;null;50
Plafon Accord Cilíndrico;5012;12;null;null;60
Plafon Accord Cilíndrico;5013;15;null;null;70
Plafon Accord Cilíndrico;546;15;null;null;80
Plafon Accord Cilíndrico;5014;15;null;null;90
Plafon Accord Cilíndrico;547;15;null;null;100
Plafon Accord Barril;5038;15;null;null;50
Plafon Accord Barril;5039;15;null;null;60
Plafon Accord Barril;5040;15;null;null;70
Plafon Accord Barril;5041;15;null;null;80
Plafon Accord Barril;5042;15;null;null;90
Plafon Accord Ripado;5033;15;null;null;50
Plafon Accord Ripado;5034;15;null;null;60
Plafon Accord Ripado;5035;15;null;null;70
Plafon Accord Ripado;5036;15;null;null;80
Plafon Accord Ripado;5037;15;null;null;90
Plafon Accord Physalis;5063;15;null;null;50
Plafon Accord Facetado;590;13;null;null;43
Plafon Accord Facetado;594;13;null;null;60
Plafon Accord Facetado;595;13;null;null;70
Plafon Accord Cristais;5025;15;null;null;50
Plafon Accord Cristais;5026;15;null;null;60
Plafon Accord Cristais;5046;15;35;35;null
Plafon Accord Cristais;5027;15;45;45;null
Plafon Accord Cristais;5028;15;55;55;null
Plafon Accord Cristais;5029;15;65;65;null
Plafon Accord Meio Squadro;587;12;40;40;null
Plafon Accord Meio Squadro;586;12;50;50;null
Plafon Accord Meio Squadro;584;12;60;60;null
Plafon Accord Meio Squadro;587CO;12;40;40;null
Plafon Accord Meio Squadro;586CO;12;50;50;null
Plafon Accord Meio Squadro;584CO;12;60;60;null
Plafon Accord Clean;573;12;30;30;null
Plafon Accord Clean;574;12;40;40;null
Plafon Accord Clean;490;12;50;50;null
Plafon Accord Meio Squadro;495;12;30;30;null
Plafon Accord Meio Squadro;494;12;40;40;null
Plafon Accord Meio Squadro;493;12;50;50;null
Plafon Accord Clean;577;12;20;20;null
Plafon Accord Clean;539;12;30;30;null
Plafon Accord Clean;565;12;40;40;null
Plafon Accord Clean;566;12;50;50;null
Plafon Accord Clean;5060;18;40;20;null
Plafon Accord Clean;5062;18;40;40;null
Plafon Accord Clean;5061;18;60;20;null
Arandela Accord Clean;437;60;20;8,5;null
Arandela Accord Clean;465;47;13;8,5;null
Arandela Accord Meio Squadro;403;55;20;8,5;null
Arandela Accord Pastilhada;439;55;15;9,5;null
Arandela Accord Pastilhada;443;30;20;9,5;null
Arandela Accord Pastilhada;441;30;12;9,5;null
Arandela Accord Patterns;4054;55;15;9,5;null
Arandela Accord Patterns;4052;30;12;9,5;null
Arandela Accord Clean;444;30;20;9,5;null
Arandela Accord Clean;434;39;30;9,5;null
Arandela Accord Clean;429;46;23;11;null
Arandela Accord Clean;454;100;20;12;null
Arandela Accord Clean;436;100;20;8,5;null
Arandela Accord Clean;428;30;15;8,5;null
Arandela Accord Clean;446;30;15;8,5;null
Arandela Accord Clean;433;30;20;8,5;null
Arandela Accord Clean;580;30;15;8,5;null
Arandela Accord Barril;4040;70;25;14;null
Arandela Accord Barril;4041;100;28;14;null
Arandela Accord Barril;4014;34;20;14;null
Arandela Accord Ripada;420;27;17;11;null
Arandela Accord Ripada;4039;16;40;10;null
Arandela Accord Ripada;440;30;20;8,5;null
Arandela Accord Ripada;471;50;20;8,5;null
Arandela Accord Ripada;472;90;20;8,5;null
Arandela Accord Ripada;470;120;20;8,5;null
Arandela Accord Ripada;456;30;23;11;null
Arandela Accord Patterns;448;30;15;8,5;null
Arandela Accord Patterns;457;120;20;8,5;null
Arandela Accord Clean;407;12;35;8,5;null
Arandela Accord Clean;404;10;30;8,5;null
Arandela Accord Sfera;415;0;null;16;35
Arandela Accord Sfera;416;35;null;15;null
Arandela Accord Clean;4051;28;14;14;null
Arandela Accord Clean;4068;30;15;9,5;null
Arandela Accord Cônica;4018;25;20;10;null
Arandela Accord Cônica;4018C;25;20;10;null
Arandela Accord Clean;419;30;15;8,5;null
Arandela Accord Facetada;467;39;25;8,5;null
Arandela Accord Clean;466;55;17;8,5;null
Arandela Accord Living Hinges;4071;50;20;10;null
Arandela Accord Living Hinges;4067;100;20;10;null
Arandela Accord Living Hinges;4066;30;23;11;null
Arandela Accord Clean;4025;15;7,5;8,5;null
Arandela Accord Cristais;4026C;15;7,5;8,5;null
Arandela Accord Facetada;4064;16;16;5,2;null
Pendente Accord Diamante;1223;21;null;null;33,5
Pendente Accord Facetado;112;21;null;null;45
Pendente Accord Facetado;118;21;null;null;60
Pendente Accord Facetado;117;21;null;null;70
Pendente Accord Diamante;1224;22,5;null;null;50
Pendente Accord Diamante;1225;34;null;null;74
Pendente Accord Facetado;1226;21;null;null;40
Pendente Accord Facetado;1227;35;null;null;75
Pendente Accord Kripton;107;40;21;21;null
Pendente Accord Kripton;108;40;21;21;null
Pendente Accord Facetado;1231;40;null;null;22
Pendente Accord Clean;115;20;50;50;null
Pendente Accord Clean;114;20;70;70;null
Pendente Accord Clean;113;20;90;90;null
Pendente Accord Ripado;601;30;15;15;null
Pendente Accord Ripado;802;30;19;19;null
Pendente Accord Ripado;101;22;22;22;null
Pendente Accord Ripado;100;30;30;30;null
Pendente Accord Ripado;103;30;15;15;null
Pendente Accord Ripado;104;20;15;15;null
Pendente Accord Ripado;801;30;19;19;null
Pendente Accord Pastilhado;286;30;15;15;null
Pendente Accord Pastilhado;817;30;19;19;null
Pendente Accord Sfera;621;35;73;15;null
Pendente Accord Sfera;623;17;90;15;null
Pendente Accord Meio Squadro;274;12;40;40;null
Pendente Accord Meio Squadro;275;12;50;50;null
Pendente Accord Meio Squadro;276;12;60;60;null
Pendente Accord Meio Squadro;265;12;40;40;null
Pendente Accord Meio Squadro;239;12;50;50;null
Pendente Accord Meio Squadro;266;12;60;60;null
Pendente Accord Clean;1150;12;90;20;null
Pendente Accord Clean;1115;8;65;7;null
Pendente Accord Clean;1117;8;125;7;null
Pendente Accord Cristais;1101;25;10;10;null
Pendente Accord Cristais;1102;25;12;12;null
Pendente Accord Cristais;1103;15;null;null;50
Pendente Accord Cristais;1104;15;null;null;60
Pendente Accord Cristais;1123;15;35;35;null
Pendente Accord Cristais;1105;15;45;45;null
Pendente Accord Cristais;1106;15;55;55;null
Pendente Accord Cristais;1107;15;65;65;null
Pendente Accord Barril;303;34;null;null;15
Pendente Accord Barril;304;70;null;null;17
Pendente Accord Barril;305;100;null;null;17
Pendente Accord Barril;306;150;null;null;30
Pendente Accord Barril;1110;19;null;null;50
Pendente Accord Barril;1111;19;null;null;60
Pendente Accord Barril;1112;19;null;null;70
Pendente Accord Barril;1113;19;null;null;80
Pendente Accord Barril;1114;19;null;null;90
Pendente Accord Ripado;1092;22;null;null;50
Pendente Accord Ripado;1093;22;null;null;60
Pendente Accord Ripado;1094;22;null;null;70
Pendente Accord Ripado;1095;22;null;null;80
Pendente Accord Ripado;1096;22;null;null;90
Pendente Accord Versátil;1243;35;null;null;68
Pendente Accord Versátil;1244;48;null;null;85
Pendente Accord Living Hinges;1239;25;null;null;14,5
Pendente Accord Living Hinges;1240;28,5;null;null;18
Pendente Accord Living Hinges;1241;41;null;null;26
Pendente Accord Living Hinges;1242;60;null;null;40
Pendente Accord Ripado;1036;50;null;null;30
Pendente Accord Ripado;1021;70;null;null;51
Pendente Accord Stecche Di Legno;1136;50;null;null;40
Pendente Accord Stecche Di Legno;1253;140;null;null;128
Pendente Accord Stecche Di Legno;1137;50;null;null;24
Pendente Accord Stecche Di Legno;1183;30;null;null;27
Pendente Accord Stecche Di Legno;1182;30;null;null;29
Pendente Accord Stecche Di Legno;1255;80;null;null;79
Pendente Accord Stecche Di Legno;1181;30;null;null;35
Pendente Accord Stecche Di Legno;1237;22;null;null;21,5
Pendente Accord Stecche Di Legno;1180;26;null;null;27
Pendente Accord Stecche Di Legno;1236;22;null;null;18
Pendente Accord Stecche Di Legno;1141;30;null;null;15
Pendente Accord Stecche Di Legno;1217;25;null;null;47,5
Pendente Accord Stecche Di Legno;1185;42;null;null;80
Pendente Accord Stecche Di Legno;1140;30;null;null;20
Pendente Accord Stecche Di Legno;1134;37;null;null;47,5
Pendente Accord Stecche Di Legno;1254;62;null;null;80
Pendente Accord Stecche Di Legno;1139;70;null;null;30
Pendente Accord Stecche Di Legno;1273;16;null;null;11